# **Portfolio**

<hr>

<ul>
    <li>En tant qu'utilisateur je veux avoir un menu pour naviguer rapidement la catégorie qui m'interesse</li>
    <li>En tant qu'utilisateur je veux pouvoir voir la liste des projets et des competences maitrisés</li>
        <li>En tant qu'utilisateur je veux pouvoir contacter l'auteur de portfolio directement via le site</li>
    <li>En tant que formateur je veux avoir une maquette du portfolio responsive dans le fichier README.MD</li>
</ul>

<hr>

# Maquette 

![maquette](img/Portfolio.png "test")



<hr>

# Maquette Mobile

![Maquette mobile](img/Portfolio-mobile.png)